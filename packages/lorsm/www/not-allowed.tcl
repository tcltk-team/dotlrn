ad_page_contract {
    
    Action not allowed.

    @author Lars Pind (lars@pinds.com)
    @creation-date 2002-10-14
    @cvs-id $Id: not-allowed.tcl,v 1.3 2005/05/17 17:03:40 miguelm Exp $
}

set context [list "[_ lorsm.Not_Allowed]"]
