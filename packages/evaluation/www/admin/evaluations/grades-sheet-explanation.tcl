# /packages/evaluation/www/admin/evaluations/grades-sheet-explanation.tcl

ad_page_contract {

	@author jopez@galileo.edu
    @creation-date May 2004
    @cvs-id $Id: grades-sheet-explanation.tcl,v 1.6 2006/08/08 21:26:42 donb Exp $

} {
	task_id:integer,notnull
}

set page_title "[_ evaluation.lt_Grades_Sheet_Explanat]"
set context [list [list "[export_vars -base student-list { task_id }]" "[_ evaluation.Studen_List_]"] "[_ evaluation.lt_Grades_Sheet_Explanat]"]

ad_return_template
