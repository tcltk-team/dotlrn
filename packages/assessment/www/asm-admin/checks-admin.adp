<master>
<property name="title">@title@ #assessment.Administration#</property>
<property name="context">@context;noquote@</property>
<br>
<b>On Request (section check)</b>
<br>
<listtemplate name="or_checks"></listtemplate>
<br>
<b>#assessment.after_this_asm# (section check)</b>
<br>
<listtemplate name="sa_checks"></listtemplate>
<br>
<b>#assessment.after_this_asm#</b>
<br>
<listtemplate name="aa_checks"></listtemplate>
<br>
<b>#assessment.immediately#</b>
<listtemplate name="i_checks"></listtemplate>
<br>
<b>#assessment.manually#</b>
<listtemplate name="m_checks"></listtemplate>
<br>

<b>#assessment.branches#</b>
<listtemplate name="branches"></listtemplate>
<br>
