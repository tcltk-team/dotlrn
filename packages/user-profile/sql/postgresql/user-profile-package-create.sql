--
-- Create the User Profile package
--
-- @author <a href="mailto:yon@openforce.net">yon@openforce.net</a>
-- @version $Id: user-profile-package-create.sql,v 1.2 2002/07/12 21:26:48 chak Exp $
--


select define_function_args ('user_profile_rel__new','rel_id,rel_type;user_profile_rel,group_id,user_id,creation_user,creation_ip');

select define_function_args ('user_profile_rel__delete','rel_id');


create function user_profile_rel__new(integer,varchar,integer,integer,integer,varchar)
returns integer as '
DECLARE
	p_rel_id		alias for $1;
	p_rel_type		alias for $2;
	p_group_id		alias for $3;
	p_user_id		alias for $4;
	p_creation_user		alias for $5;
	p_creation_ip		alias for $6;
        v_rel_id                membership_rels.rel_id%TYPE;
        v_group_id              groups.group_id%TYPE;
BEGIN
        if p_group_id is null then
            select min(group_id)
            into v_group_id
            from profiled_groups
            where profile_provider = (select min(impl_id)
                                      from acs_sc_impls
                                      where impl_name = ''user_profile_provider'');
        else
             v_group_id := p_group_id;
        end if;

        v_rel_id := membership_rel__new(
            p_rel_id,
            p_rel_type,
            v_group_id,
            p_user_id,
	    ''approved'',
            p_creation_user,
            p_creation_ip
        );

        insert
        into user_profile_rels
        (rel_id)
        values
        (v_rel_id);

        return v_rel_id;
END;
' language 'plpgsql';


create function user_profile_rel__delete(integer)
returns integer as '
DECLARE
	p_rel_id		alias for $1;
BEGIN
        delete
        from user_profile_rels
        where rel_id = p_rel_id;

	PERFORM membership_rel__delete(p_rel_id);
	
	return 0;
END;
' language 'plpgsql';
