<master>
  <property name="title">@page_title@</property>
  <property name="context">@context;noquote@</property>
  <property name="focus">locale_editing.country</property>

<p>Please update the necesary fields and press &quot;Submit&quot;</p>
<p />
<p>Editing locale <strong>@locale_label@</strong> [ <tt>@locale@</tt> ]</p>
<formtemplate id="locale_editing"></formtemplate>

