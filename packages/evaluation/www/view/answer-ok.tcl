# /packages/evaluation/www/answer-ok.tcl

ad_page_contract {
	
    Displays a success message to the student

    @author jopez@galileo.edu
    @creation-date Oct 2004
    @cvs-id $Id: answer-ok.tcl,v 1.2 2006/08/08 21:26:43 donb Exp $

} -query {
	return_url:notnull
}

ad_return_template
