<master>
  <property name="title">@page_title@</property>
  <property name="context">@context;noquote@</property>

<p />

<p>Deleting locale <strong>@locale_label@</strong> [ <tt>@locale@</tt> ]</p>

<form action="locale-delete">
@form_export_vars;noquote@
<input type="submit" value="Confirm delete">
</form>



