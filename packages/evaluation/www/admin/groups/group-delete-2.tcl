# /packabes/evaluation/www/admin/groups/group-delete-2.tcl

ad_page_contract {
	Deletes a task group

	@author jopez@galileo.edu
	@creation-date Mar 2004
	@cvs-id $Id: group-delete-2.tcl,v 1.8 2006/08/08 21:26:42 donb Exp $
} {
	evaluation_group_id:integer
	task_id:integer
	operation
	return_url
}

if { [string eq $operation "[_ evaluation.lt_Yes_I_really_want_to__2]"] } {
    db_transaction {

		evaluation::delete_evaluation_group -group_id $evaluation_group_id
		
    } on_error {
		ad_return_error "[_ evaluation.lt_Error_deleting_the_ev]" "[_ evaluation.lt_We_got_the_following_]"
		ad_script_abort
    }
} else {
	# it is a "don't do anything" request
	ad_returnredirect $return_url
}

db_release_unused_handles

ad_returnredirect "one-task?[export_vars -url { task_id }]"
