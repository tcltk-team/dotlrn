ad_page_contract {
    
    Index page for lorsm.  Redirect to the admin pages.

    @author Don Baccus (dhogaza@pacifier.com)
    @creation-date 2007-19-14
    @cvs-id $Id: index.tcl,v 1.11 2007/01/20 02:09:59 donb Exp $
}

ad_returnredirect admin
