# /packages/dotlrn-portlet/tcl/apm-callback-procs.tcl

ad_library {

     dotlrn-portlet APM callbacks library

     Procedures that deal with installing.

     @creation-date July 2004
     @author  Hector Amado (hr_amado@galileo.edu)
     @cvs-id $Id: apm-callback-procs.tcl,v 1.2 2004/07/24 08:34:16 jeffd Exp $
     
}

namespace eval dotlrn_portlet {}
namespace eval dotlrn_portlet::apm {}

ad_proc -private dotlrn_portlet::apm::after_install {
} {
  Gran permission to dotlrn-admin
} {


       set group_id [db_string group_id_from_name "
            select group_id from groups where group_name='dotlrn-admin'" -default ""]
        if {![empty_string_p $group_id] } {

        #Admin privs
        permission::grant \
             -party_id $group_id \
	     -object_id [apm_package_id_from_key dotlrn-portlet]  \
             -privilege "admin"
       } 

}

ad_proc -private dotlrn_portlet::apm::before_uninstall {
} {
  Revoke the group permissions, dotlrn-admin
} {

       set group_id [db_string group_id_from_name "
            select group_id from groups where group_name='dotlrn-admin'" -default ""]
        if {![empty_string_p $group_id] } {

        permission::revoke \
            -party_id $group_id \
            -object_id [apm_package_id_from_key dotlrn-portlet]  \
            -privilege "admin"
       }
}

