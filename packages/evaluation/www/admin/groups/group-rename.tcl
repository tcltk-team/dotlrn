# /packages/evaluation/www/admin/groups/group-rename.tcl

ad_page_contract {
	Renames a group.

	@author jopez@galileo.edu
	@creation-date Mar 2004
	@cvs-id $Id: group-rename.tcl,v 1.6 2006/08/08 21:26:42 donb Exp $
} {
	evaluation_group_id:integer
	task_id:integer
	group_name:notnull
}

db_dml rename_group { *SQL* }		

ad_returnredirect "one-group?[export_vars -url { task_id evaluation_group_id }]"

