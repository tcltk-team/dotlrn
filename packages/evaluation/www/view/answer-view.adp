<master>
<property name=title>@page_title;noquote@</property>
<property name=context>@context;noquote@</property>
<h1 class=blue>@page_title;noquote@</h1>
<br>
<br>
<div id="evaluations">
<table class="title" width=100%>
<table width="100%" style="border:0px" alt="" cellpadding="0" cellspacing="0" height="40">
  <tr>
    <td valign="middle" width="30%" style="padding-left: 10px;"><text class="blue"><b>@name;noquote@ #evaluation.answers_#</b></text></td>
  </tr>
</table>
</div>
<listtemplate name="answers"></listtemplate>
<br>
<a href=@submitted_date_url@ class=button>@submitted_date;noquote@</a>
