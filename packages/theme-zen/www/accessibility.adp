<master>
<property name="context">@context@</property>
<property name="title">@title@</property>

<h1>#theme-zen.Accessibility#</h1>

<h2>#theme-zen.Standards_Compliance#</h2>
<p>#theme-zen.Standards_compliance_text#</p>

<h2>#theme-zen.Change_Contrast#</h2>

<p>#theme-zen.The_following_styles_are_#</p>

<ul>
  <li>
    <a href="#" onclick="setActiveStyleSheet('std'); return false;"
      title="#theme-zen.switch_to_default_colors#">
      #theme-zen.Default_Colors#
    </a>
  </li>
  <li>
    <a href="#" onclick="setActiveStyleSheet('highContrast'); return false;"
      title="#theme-zen.switch_to_high_contrast#">
      #theme-zen.High_Contrast#
    </a>
  </li>
</ul>

<h2>#theme-zen.Access_keys#</h2>
<p>#theme-zen.Access_keys_can_be_used_#</p>

<listtemplate name="zen_keys"></listtemplate>


