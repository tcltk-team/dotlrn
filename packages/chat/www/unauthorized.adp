<!--
    Display unauthorized message.

    @author David Dao (ddao@arsdigita.com)
    @creation-date November 24, 2000
    @cvs-id $Id: unauthorized.adp,v 1.5 2006/03/14 12:16:09 emmar Exp $
-->
<master>
<property name="context">@context_bar;noquote@</property>
<property name="title">#chat.Unauthorized#</property>

#chat.You_dont_have_permission_room#. 
