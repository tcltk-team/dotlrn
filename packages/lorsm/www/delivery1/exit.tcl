# packages/lorsm/www/delivery/exit.tcl

ad_page_contract {
    
    Student tracking exit
    
    @author Ernie Ghiglione (ErnieG@mm.st)
    @creation-date 2004-05-25
    @arch-tag 04aa013e-2a53-45eb-825d-d576ea35cd14
    @cvs-id $Id: exit.tcl,v 1.2 2005/05/17 17:03:40 miguelm Exp $
} {
    track_id:integer
    return_url
} -properties {
} -validate {
} -errors {
}

# stamps the time when leaving the delivery environment

if {$track_id != 0} {
    lorsm::track::exit -track_id $track_id
}

# redirects
ad_returnredirect $return_url
