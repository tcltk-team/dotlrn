ad_library {

    dotlrn-homework notifications

    @creation-date 2002-07-24
    @author Don Baccus (dhogaza@pacifier.com)
    @cvs-id $Id: dotlrn-homework-notifications.tcl,v 1.1.1.1 2003/02/17 20:56:15 donb Exp $

}

namespace eval dotlrn_homework::notification {

   # We don't really use either of these at the moment though this might change
   # in the future ...

    ad_proc -public get_url {
        object_id
    } {

    }

    ad_proc -public process_reply {
        reply_id
    } {
    }

}
