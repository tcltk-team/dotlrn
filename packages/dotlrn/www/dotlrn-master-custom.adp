<master src="@dotlrn_master@">
  <if @doc_type@ not nil><property name="doc_type">@doc_type;noquote@</property></if>
  <if @title@ not nil><property name="title">@title;noquote@</property></if>

  <if @doc@ defined><property name="&doc">doc</property></if>

  <if @context@ not nil><property name="context">@context;noquote@</property></if>
  <if @context_bar@ not nil><property name="context_bar">@context_bar;noquote@</property></if>
  <if @focus@ not nil><property name="focus">@focus;noquote@</property></if>
  <if @link_control_panel@ not nil><property name="link_control_panel">@link_control_panel;noquote@</property></if>
  <if @hide_feedback@ not nil><property name="hide_feedback">@hide_feedback@</property></if>
  <if @portal_page_p@ not nil><property name="portal_page_p">@portal_page_p;noquote@</property></if>

  <slave>
