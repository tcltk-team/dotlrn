<master>
<property name="title">@page_title;noquote@</property>
<property name="context">@context;noquote@</property>

<listtemplate name="objects"></listtemplate>

<p>#assessment.unhappy_search# <a href="@search_again_url@">#assessment.try_again#</a></p>