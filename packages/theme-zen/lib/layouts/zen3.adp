<div id="main">
  <div id="main-content">
    <div class="main-content-padding">
      <list name="element_ids_2">
        <include src="@element_src@"
          element_id="@element_ids_2:item@"
          element_num="@element_ids_2:rownum@"
          element_first_num="@element_2_first_num@"
          action_string="@action_string@"
          theme_id="@theme_id@"
          region="2"
          portal_id="@portal_id@"
          edit_p="@edit_p@"
          return_url="@return_url@"
          hide_links_p="@hide_links_p@"
          page_id="@page_id@"
          layout_id="@layout_id@">
      </list>
    </div> <!-- /main-content-padding -->
  </div> <!-- /main-content -->

<div id="sidebar-1">
  <div class="sidebar-1-padding">
    <list name="element_ids_1">
      <include src="@element_src@"
        element_id="@element_ids_1:item@"
        element_num="@element_ids_1:rownum@"
        element_first_num="0"
        action_string="@action_string@"
        theme_id="@theme_id@"
        region="1"
        portal_id="@portal_id@"
        edit_p="@edit_p@"
        return_url="@return_url@"
        hide_links_p="@hide_links_p@"
        page_id="@page_id@"
        layout_id="@layout_id@">
    </list>
  </div> <!-- /sidebar-1-padding -->
</div> <!-- /sidebar-1 -->
</div> <!-- /main -->
      
<div id="sidebar-2">
  <div class="sidebar-2-padding">
    <list name="element_ids_3">
      <include src="@element_src@"
        element_id="@element_ids_3:item@"
        element_num="@element_ids_3:rownum@"
        element_first_num="@element_3_first_num@"
        action_string="@action_string@"
        theme_id="@theme_id@"
        region="3"
        portal_id="@portal_id@"
        edit_p="@edit_p@"
        return_url="@return_url@"
        hide_links_p="@hide_links_p@"
        page_id="@page_id@"
        layout_id="@layout_id@">
    </list>
  </div> <!-- /sidebar-2-padding -->
</div> <!-- /sidebar-2 -->
