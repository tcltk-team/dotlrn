<master>
<property name="title">@page_title;noquote@</property>
<property name="context">@context;noquote@</property>

<formtemplate id="task"></formtemplate>

<if @return_url@ ne "">
  <p><a href="@return_url@">#evaluation.Go_Back#</a></p>
</if>


