<master src="master">
  <property name="title">#dotlrn-homework.lt_Contents_of_folder_na#</property>
  <property name="context_bar">@context_bar;noquote@</property>

<table border="0" cellpadding="2" cellspacing="2" width="100%">
  <include src="folder-chunk" admin_actions_p="@admin_actions_p@" show_upload_url_p="@show_upload_url_p@" show_header_p="1"
           admin_p="@admin_p@" min_level="@min_level@" max_level="@max_level@" list_of_folder_ids="@list_of_folder_ids@"
           package_id="@package_id@">
</table>

