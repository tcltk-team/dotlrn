# packages/dotlrn/www/help/help-master.tcl

ad_page_contract {
    
    help master template
    
    @author Emmanuelle Raffenne (eraffenne@gmail.com)
    @creation-date 2007-10-12
    @cvs-id $Id: help-master.tcl,v 1.1 2009/07/16 09:37:28 emmar Exp $
} {
    
} -properties {
} -validate {
} -errors {
}

template::head::add_css -href "/resources/dotlrn/help.css" -media "screen"

set doc(title) $title
